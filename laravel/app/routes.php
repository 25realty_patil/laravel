<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/', function()
{
	return View::make('hello');
});
Route::get('names', function()
{
    return array(
      1 => "John",
      2 => "Mary",
      3 => "Steven"
    );
});*/

Route::get('test/one','TestController@one');
//Route::get('login/auth','LoginController@auth');
Route::get('auth','AuthController@login');
Route::get('property','PropertyController@index');
Route::get('property/deleteproperty','PropertyController@deleteproperty');