<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tbl_user';

	public function getLogin($user,$pass)
	{
		$user = DB::table('tbl_user')
					->where('user', $user)
					->where('pass', md5($pass))
					->first();
		
		return $user;
	}
}
