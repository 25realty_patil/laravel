<?php
	
class Property extends Eloquent {

//use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tbl_property';

	public function getData()
	{
		
		$property = DB::table('tbl_property')->get();
		
		return $property;
	}
	
	public function getDelete($id)
	{
		$data = DB::table('tbl_property')->where('id', '=', $id)->delete();
		
		//$data= DB::table('tbl_property')->get();
		
		return $data;
	}
}
